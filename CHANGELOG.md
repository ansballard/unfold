# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [0.4.2] - 2018-10-21
### Changed
- Update dependency libraries

## [0.4.1] - 2018-05-14
### Changed
- Update dependency libraries

## [0.4.0] - 2017-10-08
### Changed
- Update dependency libraries

## [0.3.5] - 2017-04-25
### Added
- [Autoprefixer](https://github.com/postcss/autoprefixer)

## [0.3.4] - 2015-07-06
### Changed
- Switch from jshint to eslint

## [0.3.3] - 2015-06-17
### Changed
- Update dependency libraries

## [0.3.2] - 2015-06-07
### Changed
- Switch filesystem mock library to [mock-fs](https://github.com/tschaub/mock-fs)

## [0.3.1] - 2015-06-06
### Fixed
- Update modules so `npm install` works on IO.js
